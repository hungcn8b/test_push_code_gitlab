package com.example.sqlite_order.model;

import java.io.Serializable;

public class Order implements Serializable {
    private int id;
    private String itemName;
    private String dateOrder;
    private float price;
    private float ratingOrder;

    public Order() {
    }

    public Order(int id, String itemName, String dateOrder, float price, float ratingOrder) {
        this.id = id;
        this.itemName = itemName;
        this.dateOrder = dateOrder;
        this.price = price;
        this.ratingOrder = ratingOrder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getRatingOrder() {
        return ratingOrder;
    }

    public void setRatingOrder(float ratingOrder) {
        this.ratingOrder = ratingOrder;
    }
}
