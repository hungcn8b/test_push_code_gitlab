package com.example.sqlite_order;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.sqlite_order.model.Order;

import java.util.Calendar;

public class Activity_add extends AppCompatActivity implements View.OnClickListener {
    private EditText nameOrder,price,dateOrder;
    private Button btnDate, btnAdd,btnCanCel;
    private RatingBar ratingBar;
    private SQLiteOrderHelper sqLiteOrderHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        init();
        btnAdd.setOnClickListener(v -> {
            sqLiteOrderHelper=new SQLiteOrderHelper(this);
            Order order=getData();
            sqLiteOrderHelper.addOrder(order);
            Toast.makeText(this,"Them Thanh Cong",Toast.LENGTH_SHORT).show();
            finish();
        });
        btnCanCel.setOnClickListener(v -> {
            finish();
        });

        btnDate.setOnClickListener(this);
    }
    public void init(){
        nameOrder=findViewById(R.id.nameOrder);
        price=findViewById(R.id.price);
        dateOrder=findViewById(R.id.dateOrder);
        btnDate=findViewById(R.id.btnDate);
        btnAdd=findViewById(R.id.btnAdd);
        btnCanCel=findViewById(R.id.btnCancel);
        ratingBar=findViewById(R.id.rateNum);
    }
    public Order getData(){
    Order order=new Order();
    order.setItemName(nameOrder.getText().toString());
    order.setPrice(Float.parseFloat(price.getText().toString()));
    order.setDateOrder(dateOrder.getText().toString());
    order.setRatingOrder(ratingBar.getRating());
    return order;
    }

    @Override
    public void onClick(View v) {
        if(v==btnDate){
            Calendar c=Calendar.getInstance();
            int y=c.get(Calendar.YEAR);
            int m=c.get(Calendar.MONTH);
            int d=c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    dateOrder.setText(dayOfMonth+"/"+month+"/"+year);
                }
            },y,m,d);
            datePickerDialog.show();
        }
    }
}
