package com.example.sqlite_order;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlite_order.model.Order;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {
    private List<Order> mlist;
    private View v;

    public OrderAdapter(List<Order> mlist) {
        this.mlist = mlist;
        notifyDataSetChanged();
    }

    public List<Order> getMlist() {
        return mlist;
    }

    public void setMlist(List<Order> mlist) {
        this.mlist = mlist;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_card, parent, false);
        return new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
            Order order=mlist.get(position);
            holder.name.setText(order.getId()+" - "+order.getItemName());
            holder.dateOrder.setText("ngày : "+order.getDateOrder());
            holder.price.setText("giá : "+order.getPrice());
            holder.numRate.setText("Số sao: "+order.getRatingOrder());
            holder.itemView.setOnClickListener(v1 -> {
                Order ordersent=mlist.get(position);
                Intent intent=new Intent(v.getContext(),Activity_update_delete.class);
                intent.putExtra("order",ordersent);
                v.getContext().startActivity(intent);
            });
    }

    @Override
    public int getItemCount() {
        if(mlist!=null) return mlist.size();
        return 0;
    }

    class OrderViewHolder extends RecyclerView.ViewHolder {
        private TextView name, dateOrder, price, numRate;

        public OrderViewHolder(@NonNull View v) {
            super(v);
            name = v.findViewById(R.id.nameOrder);
            dateOrder = v.findViewById(R.id.dateOrder);
            price = v.findViewById(R.id.gia);
            numRate = v.findViewById(R.id.soSao);
        }
    }
}
