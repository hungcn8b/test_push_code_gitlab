package com.example.sqlite_order;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.SearchView;

import com.example.sqlite_order.model.Order;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private SearchView searchView;
    private RecyclerView recyclerView;
    private FloatingActionButton btnAdd;

    private SQLiteOrderHelper sqLiteOrderHelper;
    private OrderAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        btnAdd.setOnClickListener(v -> {
            Intent intent = new Intent(this, Activity_add.class);
            startActivity(intent);
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                sqLiteOrderHelper = new SQLiteOrderHelper(getApplicationContext());
                List<Order> list=sqLiteOrderHelper.getOrderByName(newText);
                adapter.setMlist(list);
                recyclerView.setAdapter(adapter);
                return true;
            }
        });
    }

    public void init() {
        searchView = findViewById(R.id.searchview);
        recyclerView = findViewById(R.id.recycleView);
        btnAdd = findViewById(R.id.addbutton);
    }

    public void setData() {
        sqLiteOrderHelper = new SQLiteOrderHelper(this);
        List<Order> orderList = sqLiteOrderHelper.getAllOrder();
        adapter = new OrderAdapter(orderList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }
}