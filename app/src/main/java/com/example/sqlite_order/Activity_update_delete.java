package com.example.sqlite_order;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.sqlite_order.model.Order;

import java.util.Calendar;

public class Activity_update_delete extends AppCompatActivity implements View.OnClickListener {
    private EditText nameOrder, price, dateOrder;
    private Button btnDate, btnUpdate, btnDelete, btnCanCel;
    private RatingBar ratingBar;

    private Order orderGet;
    private SQLiteOrderHelper sqLiteOrderHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_delete);
        init();
        btnDate.setOnClickListener(this);
        getDataIntent();

        btnUpdate.setOnClickListener(v -> {
            sqLiteOrderHelper = new SQLiteOrderHelper(this);
            Order order = getData();
            order.setId(orderGet.getId());
            sqLiteOrderHelper.updateOrder(order);
            Toast.makeText(this, "Update Thành Công", Toast.LENGTH_SHORT).show();
            finish();
        });
        btnCanCel.setOnClickListener(v -> {finish();});
        btnDelete.setOnClickListener(v -> {
            sqLiteOrderHelper = new SQLiteOrderHelper(this);
            sqLiteOrderHelper.deleteOrder(orderGet.getId());
            finish();
        });
    }

    public void init() {
        nameOrder = findViewById(R.id.nameOrder);
        price = findViewById(R.id.price);
        dateOrder = findViewById(R.id.dateOrder);
        btnDate = findViewById(R.id.btnDate);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnDelete = findViewById(R.id.btnDelete);
        btnCanCel = findViewById(R.id.btnCancel);
        ratingBar = findViewById(R.id.numstar);
    }

    public void getDataIntent() {
        Intent intent = getIntent();
        orderGet = (Order) intent.getSerializableExtra("order");

        nameOrder.setText(orderGet.getItemName());
        price.setText(""+orderGet.getPrice());
        dateOrder.setText(orderGet.getDateOrder() + "");
        ratingBar.setRating( orderGet.getRatingOrder());

    }

    @Override
    public void onClick(View v) {
        if (v == btnDate) {
            Calendar c = Calendar.getInstance();
            int y = c.get(Calendar.YEAR);
            int m = c.get(Calendar.MONTH);
            int d = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    dateOrder.setText(dayOfMonth + "/" + month + "/" + year);
                }
            }, y, m, d);
            datePickerDialog.show();
        }
    }

    public Order getData() {
        Order order = new Order();
        order.setItemName(nameOrder.getText().toString());
        order.setPrice(Float.parseFloat(price.getText().toString()));
        order.setDateOrder(dateOrder.getText().toString());
        order.setRatingOrder(ratingBar.getRating());
        return order;
    }
}