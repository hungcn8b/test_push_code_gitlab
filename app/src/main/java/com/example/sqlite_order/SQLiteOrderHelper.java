package com.example.sqlite_order;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.sqlite_order.model.Order;

import java.util.ArrayList;
import java.util.List;

public class SQLiteOrderHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "OrderDB.db";
    private static final int DB_VERSION = 1;

    public SQLiteOrderHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE orderDB(\n" +
                "\t id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  \t itemName TEXT,\n" +
                "  dateOrder TEXT,\n" +
                "  price REAL,\n" +
                "  ratingOrder REAL\n" +
                ")";
        db.execSQL(sql);
    }

    public long addOrder(Order order) {
        ContentValues v = new ContentValues();
        v.put("itemName", order.getItemName());
        v.put("dateOrder", order.getDateOrder());
        v.put("price", order.getPrice());
        v.put("ratingOrder", order.getRatingOrder());
        SQLiteDatabase st = getWritableDatabase();
        return st.insert("orderDB", null, v);
    }

    public List<Order> getAllOrder() {
        List<Order> listOrder = new ArrayList<>();
        SQLiteDatabase statement = getReadableDatabase();
        Cursor cursor = statement.query("orderDB", null,
                null, null, null, null, null);
        while (cursor != null && cursor.moveToNext()) {
            Order order = new Order(cursor.getInt(0), cursor.getString(1),
                    cursor.getString(2), cursor.getFloat(3), cursor.getFloat(4)
            );
            listOrder.add(order);
        }
        return listOrder;
    }

    public Order getOrderById(int id) {
        String whereClause = "id=?";
        String[] whereArgs = {String.valueOf(id)};
        SQLiteDatabase st = getReadableDatabase();
        Cursor cursor = st.query("orderDB", null, whereClause, whereArgs, null, null, null);

        if (cursor.moveToNext()) {
            return new Order(cursor.getInt(0), cursor.getString(1),
                    cursor.getString(2), cursor.getFloat(3), cursor.getFloat(4));
        }
        return null;
    }

    public int updateOrder(Order order) {
        ContentValues v = new ContentValues();
        v.put("itemName", order.getItemName());
        v.put("dateOrder", order.getDateOrder());
        v.put("price", order.getPrice());
        v.put("ratingOrder", order.getRatingOrder());
        String whereClause = "id=?";
        String[] whereArgs = {String.valueOf(order.getId())};
        SQLiteDatabase st = getWritableDatabase();
        return st.update("orderDB", v, whereClause, whereArgs);
    }

    public int deleteOrder(int id) {
        String whereClause = "id=?";
        String[] whereArgs = {String.valueOf(id)};
        SQLiteDatabase st = getWritableDatabase();
        return st.delete("orderDb", whereClause, whereArgs);
    }

    public List<Order> getOrderByName(String name){
        List<Order> listOrder=new ArrayList<>();
        String whereClause="itemName like ?";
        String [] whereArgs={"%"+name+"%"};
        SQLiteDatabase st=getReadableDatabase();
        Cursor cursor=st.query("orderDb",null,whereClause,whereArgs,null,null,null);
        while (cursor!=null && cursor.moveToNext()){
            Order order = new Order(cursor.getInt(0), cursor.getString(1),
                    cursor.getString(2), cursor.getFloat(3), cursor.getFloat(4)
            );
            listOrder.add(order);
        }
    return listOrder;
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
